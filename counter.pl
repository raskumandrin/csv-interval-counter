#!/usr/bin/perl -w

use strict;

die `pod2text $0` unless @ARGV and scalar(@ARGV) == 2;

# get and check interval argumant
my $interval = $ARGV[0];
die "Error! Interval (first parameter) must be an integer value\n\n".`pod2text $0` unless $interval =~ /^\d+$/;

my $first_timestamp = 0;

my $previous_interval_number = 0;
my ($date,$previous_date) = ('','');
my ($timestamp,$previous_timestamp) = (0,0);
my $counter = 0;

my $date_init = 0;

my $day_counter = 0;

# read file
open my $fh, $ARGV[1] or die $!;
while (<$fh>) {
	chomp;
	
	my $line = $_;
	
	# get fill time from string
	($date) = ( $line =~ /^\d+,(\d+:\d+:\d+\s\w+),/ );
	# in case of heading (no valid time in string) - skip iteration
	next unless $date;

	# get hours, minutes and seconds of time
	my ($h,$m,$s,$p) = ( $line =~ /^\d+,(\d+):(\d+):(\d+)\s(\w+),/ );

	# Calculating timestamp
	if ($p eq 'AM' and $h == 12) {
		$timestamp = $s + 60*$m;
	}
	elsif ( $p eq 'AM') {
		$timestamp = $s + 60*$m + 3600*$h;
	}
	if ($p eq 'PM' and $h == 12) {
		$timestamp = $s + 60*$m + 3600*12;
	}
	elsif ( $p eq 'PM') {
		$timestamp = $s + 60*$m + 3600*(12+$h);
	}
	
	# in case of several days in input file, increase for specified days
	$timestamp += $day_counter*3600*24;

	# for the first iteration
	$first_timestamp = $timestamp unless $first_timestamp;

	# increase day (in case time in next string is less than in previous)
	if ($timestamp < $previous_timestamp) {
		# changed day
		$day_counter++;
		$timestamp += 3600*24;
	}
	$previous_timestamp = $timestamp;

	# counting interval number
	my $interval_number = ( $timestamp - $first_timestamp - ( ( $timestamp - $first_timestamp  ) % $interval ) ) / $interval + 1;
	$previous_interval_number = $interval_number unless $previous_interval_number;

	# if this interval number differs from previous, output previous time and counter value
	# else increase counter value
	if ( $interval_number > $previous_interval_number ) {
		$previous_interval_number = $interval_number;
		print "$previous_date, $counter\n";
		$counter = 1;
	}
	else {
		$previous_date = $date;
		$counter++;
	}

}
close($fh);

# output the last value of counter and last time value
print "$date, $counter\n";


=head1 NAME
  The program must read a csv file that has two fields (time stamp, packet
  number) and output to a csv file the number of packets read between a
  certain time interval (30 seconds, 60 seconds, 90 seconds etc). The time
  interval should be passed into the program. The output should also be a
  csv file with the first field being the last time stamp in the time
  interval specified and the second field should be the number of packets
  counted during that time interval.
  
  Algorythm: convert time to seconds. Divide all second into intervals (of
  specified in parameter length). Number all intervals. Count all lines of
  input file, depending on interval number.
=head1 SYNOPSYS
  counter.pl
    no parameters will cause this man output
  counter.pl 30 inputfile.csv
  Parameters:
    interval in seconds
    path to the file
  Results are printed to STDOUT.

=head1 AUTHOR
  Stas Raskumandrin <stas@raskumandrin.ru>, http://stas.raskumandrin.me

=cut