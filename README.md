We are interested in hiring you for a small application we need written. The language it is written in is up to you. The program must read a csv file that has two fields (time stamp, packet number) and output to a csv file the number of packets read between a certain time interval (30 seconds, 60 seconds, 90 seconds etc). The time interval should be passed into the program. The output should also be a csv file with the first field being the last time stamp in the time interval specified and the second field should be the number of packets counted during that time interval. 

For example:
Given the following csv input, and a 30 second interval entered
12:05:20pm, 24
12:05:25pm, 25
12:05:28pm, 29
12:05:30pm, 33
12:05:40pm, 50
12:05:50pm, 72
12:05:51pm, 80
12:05:55pm, 85
12:05:59pm, 92
12:06:10pm, 101
12:06:13pm, 113
12:06:15pm, 120
12:06:21pm, 130

the output should be a csv file that looks as follows:

12:05:50pm, 6
12:06:21pm, 7
and so on ...

Please let us know if you need more information. We are interested in a 24 hour turn around. We look forward to your response. Thank you.